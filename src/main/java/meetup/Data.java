package meetup;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.List;

public class Data {

  public static List<Person> personList;
  public static List<Card> cardList;
  public static List<Product> productList;

  public static void setup() {

    Person person1 = new Person("123456783", "Gabriel", "Farfan", "Sosa", asList(new Address("M", "gfarfan@bcp.com.pe"), new Address("D", "Urb. lugar Mz. X Lte 99")));
    Person person2 = new Person("123456773", "Gabriel", "Alvarado", "Lopez", asList(new Address("M", "plopez@bcp.com.pe")));
    Person person3 = new Person("123456763", "Luis", "Perez", "Gonzales", asList(new Address("D", "direccion domicilio dummy"), new Address("D", "Urb. lugar Mz. X Lte 99")));
    Person person4 = new Person("123456753", "Fernando", "Guerrero", "Perez", asList(new Address("M", "fguerrero@corp.com")));
    Person person5 = new Person("123456743", "Paul", "Jimenez", "Flores");
    Person person6 = new Person("123456733", "Piero", "Rios", "Fernandez");
    Person person7 = new Person("123456733", "Ana", "Bazan", "Bazan");

    personList = new ArrayList<>();
    personList.add(person1);
    personList.add(person2);
    personList.add(person3);
    personList.add(person4);
    personList.add(person5);
    personList.add(person6);
    personList.add(person7);

    Product product1 = new Product("19812345", "PEN", "003", "Cuenta de Ahorro", 234);
    Product product2 = new Product("19812346", "USD", "003", "Cuenta de Ahorro", 10);
    Product product3 = new Product("19812346", "PEN", "002", "CTS", 900);
    Product product4 = new Product("19812347", "PEN", "002", "CTS", 23);
    Product product5 = new Product("19812348", "PEN", "001", "Cuenta Corriente", 2);
    Product product6 = new Product("19812349", "USD", "003", "Cuenta de Ahorro", 456);

    productList = new ArrayList<>();
    productList.add(product1);
    productList.add(product2);
    productList.add(product3);
    productList.add(product4);
    productList.add(product5);
    productList.add(product6);

    List<Product> products1 = new ArrayList<>();
    products1.add(product1);
    products1.add(product2);
    products1.add(product3);

    List<Product> products2 = new ArrayList<>();
    products2.add(product4);
    products2.add(product5);
    products2.add(product6);

    Card card1 = new Card("45678912", person1, products1);
    Card card2 = new Card("45678913", person2, products2);

    cardList = new ArrayList<>();
    cardList.add(card1);
    cardList.add(card2);
  }

}
