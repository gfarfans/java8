package meetup;

import java.util.List;
import java.util.Objects;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class Person implements Cloneable{
  private String personId;
  private String name;
  private String fatherLastName;
  private String motherLastName;


  private List<Address> addresses;

  private City city;

  public Person(String personId, String name, String fatherLastName, String motherLastName) {
    this.personId = personId;
    this.name = name;
    this.fatherLastName = fatherLastName;
    this.motherLastName = motherLastName;
  }

  public Person(String personId, String name, String fatherLastName, String motherLastName, List<Address> addresses) {
    this.personId = personId;
    this.name = name;
    this.fatherLastName = fatherLastName;
    this.motherLastName = motherLastName;
    this.addresses = addresses;
  }

  public Person() {
    this.personId = "";
    this.name = "";
//    this.city = "";

    System.out.println("Persona Creada");
  }

  public Person(String personId, String name, City city) {
    this.personId = personId;
    this.name = name;
    this.city = city;
  }

  public Object clone(){
    Object obj=null;
    try{
      obj=super.clone();
    }catch(CloneNotSupportedException ex){
      System.out.println(" no se puede duplicar");
    }
    return obj;
  }


}
