package meetup;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Product {

  private String productNumber;
  private String currency;
  private String productCode;
  private String description;
  private double amount;
}
