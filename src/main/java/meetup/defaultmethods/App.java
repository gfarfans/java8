package meetup.defaultmethods;

public class App implements ICard, IProduct {


  @Override
  public void getProduct() {

  }

  @Override
  public void getCard() {

  }

  @Override
  public void metodo() {
    IProduct.super.metodo();

  }

  public static void main(String[] args) {
    App app = new App();
    app.metodo();


  }



}



