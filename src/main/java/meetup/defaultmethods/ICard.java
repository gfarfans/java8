package meetup.defaultmethods;


public interface ICard {

  void getCard();  //METODO ABSTRACTO

  default void metodo() {
    System.out.println("Metodo ICARD");
  }


}
