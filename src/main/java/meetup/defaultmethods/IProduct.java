package meetup.defaultmethods;

public interface IProduct {

  void getProduct();

  default void metodo() {
    System.out.println("Metodo IPRODUCT");
  }


}
