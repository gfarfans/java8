package meetup.functionalinterface;


public class App {

  public static void main(String[] args) {

    IFunctional sumaNumeros = (f, g) -> {
      //IMPLEMENTACION
      System.out.println("Interfaz funcional");
      int x = f + g;
      return x;
    };

    System.out.println(sumaNumeros.sum(5, 8));

  }

}
