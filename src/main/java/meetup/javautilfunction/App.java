package meetup.javautilfunction;

import meetup.Card;
import meetup.Data;
import meetup.Person;
import meetup.Product;

import java.util.List;
import java.util.function.*;

public class App {

  private static List<Person> personList;
  private static List<Card> cardList;
  private static List<Product> productList;
  private static Person person;

  public static void ini() {

    Data.setup();

    personList = Data.personList;
    cardList = Data.cardList;
    productList = Data.productList;
    person = personList.get(1);

  }



  public static void main(String[] args) {

    ini();



    Function<String, String> mayusculas = String::toUpperCase;

    System.out.println(mayusculas.apply("intelidata"));



    Function<Person, String> getNombre = Person::getName;

    System.out.println(getNombre.apply(person));


    String composing01 = getNombre.andThen(mayusculas).apply(person);
    String composing02 = mayusculas.compose(getNombre).apply(person);

    System.out.println(composing01);
    System.out.println(composing02);



    Function<Integer, Integer> duplicate = entero -> entero * 2;
    Function<Integer, Integer> square = entero -> entero * entero;

    System.out.println(duplicate.andThen(square).apply(4));
    System.out.println(duplicate.compose(square).apply(4));


    /**

    Function<String, Person> getPersonById = id -> personList.stream()
        .filter(resp -> id.equals(resp.getPersonId()))
        .findFirst().orElse(new Person());


    String result = getPersonById.andThen(getNombre).andThen(mayusculas).apply("123456751");

    System.out.println("Buscar Person por ID : " + result);





    UnaryOperator<Integer> duplicate01 = entero -> entero * 2;

    UnaryOperator<Integer> square01 = entero -> entero * entero;

    System.out.println(duplicate01.andThen(square01).apply(2));




    IntUnaryOperator duplicate02 = entero -> entero * 2;

    IntUnaryOperator square02 = entero -> entero * entero;

    System.out.println(duplicate02.compose(square02).applyAsInt(2));




    BiFunction<List<Person>, String, Person> getPerson = (perlist, id) ->
        perlist.stream().filter(resp -> id.equals(resp.getPersonId())).findFirst().orElse(new Person());

    String result02 = getPerson.andThen(getNombre).andThen(mayusculas).apply(personList, "123456781");
    System.out.println("Bifunction Buscar Person por ID : " + result02);




    Consumer<String> imprimir = System.out::println;   //accept

    imprimir.accept(getPersonById.andThen(getNombre).andThen(mayusculas).apply("123456781"));



    Supplier<Person> generar = () -> new Person("123456782", "Carlos", "Angulo", "Mendoza");

    Person pGenerada = generar.get();



    imprimir.accept(getNombre.apply(pGenerada));



    Predicate<Integer> test01 = dato -> dato > 10;

    Predicate<Integer> test02 = dato -> dato == 5;

    System.out.println(test01.or(test02).test(5));

    */


  }
}
