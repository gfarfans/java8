package meetup.lambda;

public class App {

  private int y;

  int method() {
    int x = 3;

    IPerson sumar = (a, b) -> {

      int suma = a + b;
      return suma;
    };

    return sumar.sum(2,3);
  }

  public static void main(String[] args) {

    App app = new App();

    System.out.println("Suma es : " + app.method());

  }


}
