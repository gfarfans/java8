package meetup.methodreferences;

public class App {

  public static void metodoStatic() {

    System.out.println("Metodo Estatico");
  }

  public void metodoParticular() {

    System.out.println("Metodo Particular");
  }


  public static void main(String[] args) {


    /********************************************************************
     **  04)  Referencia a constructor
     **       String::new
     */
    IReference03 ref04 = Integer::new;
    System.out.println(ref04.refMethod(4));


  }
}
