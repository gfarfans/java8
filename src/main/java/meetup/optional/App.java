package meetup.optional;

import static java.util.Optional.ofNullable;

import meetup.Address;
import meetup.Card;
import meetup.Data;
import meetup.Person;
import meetup.Product;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class App {

  private static List<Person> personList;
  private static List<Card> cardList;
  private static List<Product> productList;
  private static Person person;

  public static void ini() {

    Data.setup();

    personList = Data.personList;
    cardList = Data.cardList;
    productList = Data.productList;
    person = personList.get(3);

  }



  public static void main(String[] args) {

    ini();

    Function<String, String> mayusculas = String::toUpperCase;


    // Function Busca Persona por un ID enviado como parametro

    Function<String, Person> getPersonOptional01 = id -> personList.stream()
        .filter(persona -> id.equals(persona.getPersonId()))
        .findFirst().orElseGet(Person::new);


    Person opPerson01 = getPersonOptional01.apply("123456781");
    System.out.println("OrElse: " + opPerson01.getName());


    // Function Obtiene el nombre de la persona de un Optional<Persona> enviado como paramtro de Manera Imperativa

    Function<Optional<Person>, String> optional01 = person1 -> {

      if (person1.isPresent()) {
        return person1.get().getName();
      } else {
        return "Vacio";
      }
    };



    System.out.println("Optional 01: " + optional01.apply(Optional.of(person)));


    // Optional de Manera funcional

    Function<Optional<Person>, String> optional02 = persona ->
        persona.map(Person::getName).orElse("Vacio");

    System.out.println("Optional MAP: " + optional02.andThen(mayusculas).apply(Optional.of(person)));
    System.out.println("Optional MAP: " + optional02.andThen(mayusculas).apply(Optional.empty()));


    //Data.setup();
    List<Person> personList = Data.personList;

    String emailAddress = getElectronicEmailAddress.apply(personList.get(0));
    System.out.println("Optional 04: Email ==> " + emailAddress);
  }



  private static Function<Person, String> getElectronicEmailAddress =
      person -> ofNullable(person.getAddresses())
          .map(addresses -> addresses.stream()
              .filter(address -> "M".equals(address.getType()))
              .findAny()
              .map(Address::getValue)
              .orElse("SINCORREO@EMAIL.COM"))
          .orElse("no tiene correo");

}
