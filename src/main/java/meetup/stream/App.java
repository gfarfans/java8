package meetup.stream;

import meetup.*;
import org.springframework.xml.transform.StringSource;

import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {

    private static List<Person> personList;
    private static List<Card> cardList;
    private static List<Product> productList;
    private static Person person;

    public static void ini() {

        Data.setup();

        personList = Data.personList;
        cardList = Data.cardList;
        productList = Data.productList;
        person = personList.get(1);

    }

    public List<String> listar(String type) {

        List<String> lista = new ArrayList<>();
        for (Person p : personList) {

            if (p.getPersonId().endsWith(type)) {
                lista.add(String.join(" ", p.getPersonId(), p.getName()));
            }
        }

        return lista;
    }


    public static void main(String[] args) {

        /*MANUEL*/

        System.out.println("Inicio");

        System.out.println("solución");

        String prueba = "solución";

        StringSource source = new StringSource(prueba);

        StringWriter responseWriter = new StringWriter();

        System.out.println("**** " + source);


        StreamResult result = new StreamResult(responseWriter);

        System.exit(1);

        Persona persona1 = new Persona();

        Persona persona2 = new Persona("Memo", "Herrera", "20");

        Persona persona3 = new Persona("Gabriel", "Farfan");

        persona1.setNombre("Manuel");
        persona1.setApellido("Bustamante");
        persona1.setEdad("22");

/*
    System.out.println("El nombre del usuario es = " + persona1.getDatos().toUpperCase());
    System.out.println("El nombre del usuario es = " + persona2.getDatos().toLowerCase());
    System.out.println("El nombre del usuario es = " + persona3.getDatos().replace("a","o"));

*/

        System.out.println("Programacion Funcional");

        List<Persona> personas = new ArrayList<>();

        personas.add(persona1);
        personas.add(persona2);
        personas.add(persona3);


        List<String> nombre = new ArrayList<>();


        String nombreObtenido = personas.stream()
                .map(p -> p.getNombre()).filter(p -> p.startsWith("G"))
                .findFirst().orElse("No Encontrado");

        nombre = personas.stream()
                .map(p -> p.getNombre()).filter(p -> p.startsWith("M"))
                .collect(Collectors.toList());

        System.out.println("Nombre  " + nombreObtenido);
        System.out.println("Lista " + nombre);


        System.exit(1);
        /*   MANUEL FIN*/


        BiConsumer<List<String>, String> imprime = (lista, txt) -> {
            System.out.println("\n************ " + txt + " ************");
            lista.forEach(System.out::println);
            System.out.println("********************************\n");
        };

        Function<String, String> mayusculas = String::toUpperCase;

        ini();
        App app = new App();

        List<String> personPer = app.listar("1");

        imprime.accept(personPer, "P. Imperativa");

        //ForEach
        personList.forEach(resp -> System.out.println(String.join(" - ", resp.getPersonId(), resp.getName())));


        System.out.println("YYYYYYYYYYYYYYYYYYYYYYY INICIO");
        //Map
        List<String> names = personList.stream()
                .map(persona -> persona.getName() + " " + persona.getFatherLastName())
                .collect(Collectors.toList());

        imprime.accept(names, "MAP");


        //Filter

        List<String> names01 = personList.stream()
                .filter(persona -> persona.getPersonId().endsWith("1"))
                .map(persona -> String.join(" ", persona.getPersonId(), persona.getName()))
                .collect(Collectors.toList());

        imprime.accept(names01, "FILTER");

        System.out.println("YYYYYYYYYYYYYYYYYYYYYYY FIN");


        //FindFirst
        String name02 = personList.stream()
                .filter(persona -> persona.getPersonId().endsWith("3"))
                .findFirst()
                .map(persona -> persona.getName() + " " + persona.getFatherLastName())
                .orElse("No encontrado");

        System.out.println(name02);


        //Count
        double creditCard = productList.stream()
                .filter(producto -> producto.getProductCode().equals("003"))
                .count();


        System.out.println("N. Credit Card: " + creditCard);


        //Skip Limit
        List<String> skipLimit = productList.stream()
                .skip(2)
                .limit(3)
                .map(Product::getDescription)
                .collect(Collectors.toList());

        imprime.accept(skipLimit, "SKIP - LIMIT");

        //Sorted
        List<String> ordenar = personList.stream()
                .map(Person::getName)
                .sorted()
                .collect(Collectors.toList());

        imprime.accept(ordenar, "Sorted - Ordenar");

        personList.forEach(per -> {
            System.out.println("Persona: " + per);
        });

        System.out.println("*****************************");

        Stream<Person> algo = personList.stream()
                .sorted(Comparator.comparing(Person::getName).thenComparing(Person::getFatherLastName));

        algo.forEach(per -> {
            System.out.println("Personaooooooooooooooo: " + per);
        });

        //Max
        Product productMax = productList.stream()
                .max(Comparator.comparing(Product::getAmount))
                .orElse(new Product());

        System.out.println(productMax.getAmount());

        //Min
        Product productMin = productList.stream()
                .min(Comparator.comparing(Product::getAmount))
                .orElse(new Product());

        System.out.println(productMin.getAmount());

        //Grouping
        Map<String, List<Product>> agrupa = productList.stream()
                .collect(Collectors.groupingBy(Product::getCurrency));

        System.out.println("PRUEBA ****************");
        List<Product> products = agrupa.get("USDX");
        System.out.println(products);

        agrupa.forEach((x, y) -> {

            System.out.println(x);
            System.out.println(y);
        });

        agrupa.get("USD");

        agrupa.get("USD").forEach(x -> System.out.println(x.getProductNumber() + " " + x.getCurrency() + " " + x.getDescription()));

        String s = "15/09 05:09'10''";

        LocalDate date = LocalDate.now();

        String anio = String.valueOf(date.getYear());

        s = s.replace(" ", "/" + anio + " ");
        s = s.replace("''", "");
        s = s.replace("'", ":");


        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

        LocalDateTime parse = LocalDateTime.parse(s, formatter);

        System.out.println("-> " + parse);

        System.out.println("******************************************");


        HashMap<String, Person> hola = new HashMap<>();

        hola.put("2", new Person("2", "Gabriel", new City("Piura")));

        Person pintar1 = hola.get("2");

        System.out.println("Pinta: " + pintar1);

        pintar1.getName();


        List<Integer> integerList = new ArrayList<>();

        integerList.add(1);
        integerList.add(3);
        integerList.add(4);

        System.out.println("--> " + integerList.subList(0, 2));
        System.out.println("--> " + integerList.subList(2, 3));


    }

}
