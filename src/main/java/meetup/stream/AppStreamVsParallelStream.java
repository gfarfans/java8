package meetup.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/*
A parallel stream has a much higher overhead compared to a sequential one. Coordinating the threads takes a significant amount of time. I would use sequential streams by default and only consider parallel ones if
I have a massive amount of items to process (or the processing of each item takes time and is parallelizable)
I have a performance problem in the first place
I don't already run the process in a multi-thread environment (for example: in a web container, if I already have many requests to process in parallel, adding an additional layer of parallelism inside each request could have more negative than positive effects)

*/

public class AppStreamVsParallelStream {

  public static void main(String[] args) {

    //===================================== SETUP
    List<Integer> numeros = new ArrayList<>();
    for (int i = 0; i < 10000000; i++) {
      numeros.add((int) Math.round(Math.random() * 100));
    }


    //===================================== STREAM
    long baseStart = System.currentTimeMillis();
    for (int i = 0; i < 10; i++) {
      long start = System.currentTimeMillis();
      List<Integer> filteredList = numeros.stream()
          .filter(x -> x % 2 == 0)
          .sorted()
          .collect(Collectors.toList());
      System.out.printf("%d elements computed (STREAM) in %5d msecs with %d threads\n", filteredList.size(), System.currentTimeMillis() - start, Thread.activeCount());
    }
    System.out.printf("STREAM Execution time  %5d msecs\n\n\n", System.currentTimeMillis() - baseStart);

    //===================================== PARALLEL STREAM
    baseStart = System.currentTimeMillis();
    for (int i = 0; i < 10; i++) {
      long start = System.currentTimeMillis();
      List<Integer> filteredList = numeros.parallelStream()
          .filter(x -> x % 2 == 0)
          .sorted()
          .collect(Collectors.toList());
      System.out.printf("%d elements computed (PARALLEL STREAM) in %5d msecs with %d threads\n", filteredList.size(), System.currentTimeMillis() - start, Thread.activeCount());
    }
    System.out.printf("PARALLELSTREAM Execution time  %5d msecs", System.currentTimeMillis() - baseStart);
  }
}
