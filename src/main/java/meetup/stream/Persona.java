package meetup.stream;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Persona {

    private String nombre;
    private String apellido;
    private String edad;

    public Persona(String nombre, String apellido) {
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public String getDatos() {
        return nombre + " " + apellido;
    }
}
