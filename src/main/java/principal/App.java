package principal;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class App {

    LocalDate localDate;

    public static String obtenerFechaFormato(LocalDateTime date, String formatoFecha) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formatoFecha);
        return date.format(formatter);

    }

    public static boolean createFile(String ruta, String nombreArchivo, List<String> contentList, String separador) {


        System.out.println("Inicio: " + System.currentTimeMillis());

        String rutaFinal = ruta + nombreArchivo;

        System.out.println("Inicio de creacion de archivo: " + rutaFinal);

        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(rutaFinal, true), StandardCharsets.UTF_8))) {

            StringBuilder data = new StringBuilder();
            contentList.forEach(item -> data.append(item).append(separador));
            writer.write(data.toString());

            System.out.println("Se creó el archivo con exito " + rutaFinal);

        } catch (Exception e) {
            System.out.println("Ocurrio un error en la creacion de Archivo" + rutaFinal);
            System.exit(1);

        }

        System.out.println("Fin: " + System.currentTimeMillis());

        return true;
    }

    public static boolean createFileFin(String ruta, String nombreArchivo) {

        System.out.println("Inicio Fin: " + System.currentTimeMillis());

        String rutaFinal = ruta + nombreArchivo + ".fin";

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(rutaFinal))) {

            System.out.println("Se creo el archivo: " + rutaFinal);

        } catch (Exception e) {
            System.out.println("Ocurrio un error en la creacion de Archivo");
        }
        System.out.println("Fin fin: " + System.currentTimeMillis());
        return true;
    }

    public static LocalDate addMonth(LocalDate date, int month) {

        return date.plusMonths(month);
    }

    public static List<File> searchFilesNew(String ruta) {
        System.out.println("Busqueda de Archivos con sufijo: ");

        List<File> fileList = new ArrayList<>();

        try {
            Files.list(Paths.get(ruta))
                    .filter(x -> x.toFile().isDirectory()).forEach(dir -> {
                        System.out.println(dir);
                        try {
                            Files.list(Paths.get(String.valueOf(dir)))
                                    .filter(arch -> arch.toFile().isFile())
                                    .filter(arch -> arch.toFile().getName().endsWith(".fin")).forEach(file -> {
                                        fileList.add(new File(file.toFile().getAbsolutePath().substring(0, file.toFile().getAbsolutePath().length() - 4)));

                                    });
                        } catch (Exception e) {
                            System.out.println("Error al obtener el listado de archivos Interno");
                        }
                    });
        } catch (Exception e) {
            System.out.println("Error al obtener el listado de archivos");
        }
        return fileList;
    }

    public static String formatearFecha(String fecha, String oldFormat, String newFormat) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(oldFormat, new Locale("es", "ES"));
        Date d = sdf.parse(fecha);
        sdf.applyPattern(newFormat);
        return sdf.format(d);
    }

    public static String capitalize(String word) {
        return word.substring(0, 1).toUpperCase() + word.substring(1);
    }


    public static String getDataPYC(String content, int pos) {

        String[] array = content.split(";", -1);
        String temp = "";

        try {
            temp = array[pos - 1];

        } catch (Exception e) {

            System.exit(1);
            return temp;
        }

        return temp;
    }

    public static List<Registro> getFileContent(String ruta, String nombreArchivo) {

        String rutaFinal = ruta + nombreArchivo;

        List<Registro> stringList = new ArrayList<>();

        try {

            FileReader f = new FileReader(rutaFinal);
            BufferedReader b = new BufferedReader(new InputStreamReader(new FileInputStream(rutaFinal), "UTF-8"));

//            stringList = b.lines().filter(x -> !x.trim().equals("")).collect(Collectors.toList());

            b.close();
        } catch (Exception e) {
            System.exit(1);
        }

        return stringList;
    }

    public static String getDataPipe(String content, int pos) {

        String[] array = content.split("[|]", -1);

        return array[pos - 1];
    }

    public static String getHHMMSS(String s) {

        DateTimeFormatter formatterGeneric = DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmmss");
        LocalDateTime localDateTime = LocalDateTime.parse(s, formatterGeneric);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HHmmss");

        return formatter.format(localDateTime);
    }

    public static List<List<String>> getSubList(List<String> list, int threads) {
        if (list.size() <= threads) {
            return Collections.singletonList(list);
        }
        int subListSize = (int) Math.ceil((double) list.size() / (double) threads);
        return IntStream.range(0, threads)
                .mapToObj(i -> list.subList(i * subListSize, Math.min((i + 1) * subListSize, list.size())))
                .collect(Collectors.toList());
    }

    public static String[] decodeBasicCredentials(String credentials) {
        String[] result = null;

        if (credentials != null && credentials.startsWith("Basic ")) {
            String base64Credentials = credentials.substring("Basic ".length());
            byte[] decodedBytes = Base64.getDecoder().decode(base64Credentials);
            String decodedCredentials = new String(decodedBytes, StandardCharsets.UTF_8);

            result = decodedCredentials.split(":", 2);
        }

        return result;
    }

    public static boolean writeObjectToFile(Object object, String fileName) {

        try (PrintWriter out = new PrintWriter(new FileWriter(fileName))) {
            Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
            String jsonString = gson.toJson(object);
            jsonString = jsonString.substring(1, jsonString.length() - 1);

            System.out.println("***********************");
            System.out.println(jsonString);
            out.write(jsonString);
            System.out.println("Se creo el archivo correctamente" + fileName);
            return true;
        } catch (Exception e) {
            System.out.println("Hubo un error la creacion del archivo: " + fileName);
            return false;
        }
    }

    public static String fechaActualParaWebServices() {

        String FORMAT_YYYYMMDD_T_HHMMSS_SSS_Z = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

        String fechaFinal = null;
        Date date = new Date();
        SimpleDateFormat simple = new SimpleDateFormat(FORMAT_YYYYMMDD_T_HHMMSS_SSS_Z);
        String retornaFecha = simple.format(date);
        String parteUno = retornaFecha.substring(0, retornaFecha.length() - 2);
        String parteDos = retornaFecha.substring(retornaFecha.length() - 2, retornaFecha.length());
        fechaFinal = parteUno + ":" + parteDos;
        return fechaFinal;
    }

    public static String getDateWSISO() {
        ZonedDateTime fechaHoraActual = ZonedDateTime.now();

        DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;

        return fechaHoraActual.format(formatter);
    }


    public static void main(String[] args) {

        System.out.println("Prueba");

        String pk = "ajaja*1*2*3*5";

        String[] split = pk.split("[" + "*" + "]", -1);

        int sizePk = split.length;

        System.out.println("size " + sizePk);

        String x1 = split[sizePk - 2];
        String x2 = split[sizePk - 1];

        System.out.println("x1 " + x1);
        System.out.println("x2 " + x2);



        System.exit(1);


        System.out.println(getDateWSISO());

        System.out.println(fechaActualParaWebServices());

        List<Registro> lstClientes = new ArrayList<>();

        Registro r1 = new Registro();
        r1.setLinea("linea1");
        r1.setClave("1");

        Registro r2 = new Registro();
        r2.setLinea("linea1");
        r2.setClave("2");

        Registro r3 = new Registro();
        r3.setLinea("linea1");
        r3.setClave("1");

        lstClientes.add(r1);
        lstClientes.add(r2);
        lstClientes.add(r3);

        int tamanoSubLista = 2; // Define el tamaño de las sublistas

        List<List<Registro>> listas = IntStream.range(0, (lstClientes.size() + tamanoSubLista - 1) / tamanoSubLista)
                .mapToObj(i -> lstClientes.subList(i * tamanoSubLista, Math.min(lstClientes.size(), (i + 1) * tamanoSubLista)))
                .collect(Collectors.toList());

        // 'listas' contendrá sublistas de tamaño N

        System.out.println("Cantidad de Listas = " + listas.size());

        System.out.println("Listas = " + listas);


        System.out.println("INICIO");

        String credentials = "Basic dXNyX2NjbTpDY21AZW1pc3MyMw==";

        String[] decodedCredentials = decodeBasicCredentials(credentials);

        if (decodedCredentials != null) {
            String username = decodedCredentials[0];
            String password = decodedCredentials[1];

            System.out.println("Username: " + username);
            System.out.println("Password: " + password);
        } else {
            System.out.println("Invalid credentials format.");
        }


        System.out.println("Inicio l");

        LocalDate billEndDateLD = LocalDate.of(2021, 11, 30);
        LocalDate issueDate = LocalDate.of(2021, 10, 29);

        int compare = billEndDateLD.compareTo(issueDate);


        System.out.println(compare);

//
//
//        String value = "<![CDATA[<TED version=\"1.0\"><DD><RE>76124890-1</RE><TD>33</TD><F>68669286</F><FE>2023-04-01</FE><RR>99500140-3</RR><RSR>EKA CHILE SA</RSR><MNT>2210939</MNT><IT1>Cargo Fijo</IT1><CAF version=\"1.0\"><DA><RE>76124890-1</RE><RS>TELEFONICA MOVILES CHILE S.A.</RS><TD>33</TD><RNG><D>68518851</D><H>68818850</H></RNG><FA>2023-01-25</FA><RSAPK><M>4WY330Oel6l5emyEjJQL52H/bm2KsEcZEW00iz/0nMrRUZwV+kNJ9Itqn2p55xsS/aPTr/0nEl4EopTjjFDZ+w==</M><E>Aw==</E></RSAPK><IDK>300</IDK></DA><FRMA algoritmo=\"SHA1withRSA\">WaU+NTlTmAxj9dGh9XA8SHhlvJ8iMFOyUcje9v09Fion6EREi4uxFEAoLCo/lC1NdkaoXxUqwMmWC6KBhs45uA==</FRMA></CAF><TSTED>2023-04-03T01:57:18</TSTED></DD><FRMT algoritmo=\"SHA1withRSA\">L4EGmtD9wWcC0FYm90NEUvUt3aoUwazzONfU87QzSyMa1N8yagYGg5x1GchBYT29V20du1i3+j60 4YxPaJ2esA==</FRMT></TED>]]>";
//        TransformerFactory transformerFactory = TransformerFactory.newInstance();
//        Transformer transformer = null;
//        try {
//            transformer = transformerFactory.newTransformer();
//
//            StreamSource source = new StreamSource(new StringReader(value));
//            StringWriter writer = new StringWriter();
//            StreamResult result = new StreamResult(writer);
//            transformer.transform(source, result);
//            String escapedContent = writer.toString();
//
//// Escribir el CDATA escapado en la salida
//            String output = "<DTE_RESPONSE><![CDATA[" + escapedContent + "]]></DTE_RESPONSE>";
//            value = output;
//            System.out.println("*** " + value);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


//        LocalDate date = LocalDate.now();
//        date = date.minusDays(1);
//        System.out.println("=" + date.getYear() + date.getMonthValue() + date.getDayOfMonth());
//
//        System.out.println(date);
//
//
//        String txt = "2021-08-25 18:28:49";
//
//        DateTimeFormatter formatterGeneric = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//        LocalDateTime localDateTime = LocalDateTime.parse(txt, formatterGeneric);
//
//        System.out.println("FECHA :::::::::::::::::::: " + localDateTime);


//        String start = "2022-11-01";
//
//        try {
//
//            System.out.println(start.substring(8, 10));
//
//            String mes = formatearFecha(start.substring(5, 7), "MM", "MMM");
//            System.out.println(start.substring(8, 10) + " " + capitalize(mes));
//        } catch (Exception e) {
//            System.out.println("Error");
//        }

//        System.out.println("localdate " + localDate);
//        System.out.println("" + localDate.getYear());
//        System.out.println("" + String.format("%02d", localDate.getMonthValue()));
//
//        localDate = addMonth(localDate, -1);
//
//        System.out.println("localdate " + localDate);
//        System.out.println("" + localDate.getYear());
//        System.out.println("" + String.format("%02d", localDate.getMonthValue()));


//    List<String> xx = new ArrayList<>();
//
//    xx.add("Hola");
//    xx.add("Como estas");


//        String FORMATO_FECHA_YYYYMMDD_CONSEPARADOR = "yyyy-MM-dd_HHmmss";

//        LocalDate hoy = LocalDate.now();
//        System.out.println("hoy " + hoy);
//        LocalDateTime ayer = hoy.minusDays(1);

//
//    String fechaParaProceso = obtenerFechaFormato(ayer, FORMATO_FECHA_YYYYMMDD_CONSEPARADOR);
//
//    System.out.println("Fecha: " + fechaParaProceso);
//
//    String ruta = "D:/car/xx/";
//
//    System.out.println(ruta.substring(0, ruta.indexOf("xx/".substring(0, "xx/".length()-1))));


//    try {
//      Files.list(Paths.get("D:/"))
//              .filter(x -> x.toFile().isDirectory()).forEach(dir -> {
//        System.out.println("Directorio a buscar: " + dir.getFileName());
//
//      });
//    } catch (IOException e) {
//      e.printStackTrace();
//    }

//    searchFilesNew("D:/");

//    List<String> stringList = new ArrayList<>();
//
//    for (int i = 1; i <= 5000000; i++) {
//
//      stringList.add(String.valueOf(i));
//
//    }

//   if (createFile("D:/", "Raul.txt", stringList,"\r\n")) {
//     createFileFin("D:/","Raul.txt");
//
//   }

//
//   Person persona1 = new Person();
//   persona1.setName("Gabriel");
//    Person persona2 = new Person();
//    persona2.setName("Jorge");
//    Person persona3 = new Person();
//    persona3.setName("Raul");
//    Person persona4 = new Person();
//    persona4.setName("Ivan");
//
//
//   List<Person> personList1 = new ArrayList<>();
//   List<Person> personList2 = new ArrayList<>();
//   List<Person> respuesta = new ArrayList<>();
//
//   personList1.add(persona1);
//   personList1.add(persona2);
//   personList1.add(persona3);
//
//   //Hilos
//   personList2.add(persona1);
//   personList2.add(null);
//   personList2.add(persona3);
//
//   respuesta = personList2.stream()
//           .filter(Objects::nonNull).collect(Collectors.toList());
//
//   if (personList1.size() != respuesta.size()) {
//     System.out.println("Tamaños diferentes hacer algo");
//
//     personList1.removeAll(respuesta);
//   }
//
//    System.out.println("lista 1: " + personList1);
//
//    System.out.println("Size de respuesta : " + respuesta.size());
//
//
//    respuesta.addAll(personList1);
//
//    System.out.println("Size de respuesta : " + respuesta.size());
//    System.out.println("REspuesta** " + respuesta);
//
//    System.out.println("Personalist1: " + personList1.size());
//    System.out.println("Personalist2: " + personList2.size());
//
//
//
//    Map<String, List<Person>> agrupamientoFileOutput = respuesta.stream()
//            .filter(x -> !x.getName().equals(""))
//            .collect(Collectors.groupingBy(p -> p.getName() + "-" + p.getPersonId()));
//
//    System.out.println("Final: " + agrupamientoFileOutput);
//
//    List<String> cc = new ArrayList<>();
//
//    List<String> st1 = new ArrayList<>();
//    List<String> st2 = new ArrayList<>();
//
//    for (int i = 1; i<= 30; i++) {
//      st1.add(String.valueOf(i));
//    }
//
//    for (int i = 50; i<= 150; i++) {
//      st2.add(String.valueOf(i));
//    }
//
//    List<List<String>> listaFinal = new ArrayList<>();
//    listaFinal.add(st1);
//    listaFinal.add(st2);
//
//    List<String> finalString = new ArrayList<>();
//    List<String> finalString1 = new ArrayList<>();
//
//    System.out.println("INICIO");
//    ForkJoinPool myThread = new ForkJoinPool(30000);
//
//    try {
//      myThread.submit(() -> {
//        listaFinal.parallelStream().forEach(x -> {
//          List<String> str = new ArrayList<>();
//
//          x.forEach(y -> {
//            if (y == null) {
//              System.out.println("nulo: ");
//            }
//            str.add(y);
//          });
//
//          finalString.addAll(str);
//          finalString1.addAll(str);
//
//        });
//              }
//      ).get();
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
//
//    long dd = finalString.stream().filter(Objects::isNull).count();
//    System.out.println("Nulos: " + dd);
//
//    System.out.println("tama " + finalString.size());
//    System.out.println(finalString);
//    System.out.println(finalString1);


//    City city = new City("Peru");
//
//    Person person = new Person("1","Gabriel", city);
//
//    Person person2 = (Person) person.clone();
//
//    person2.setCity((City) city.clone());
//
//    person2.setName("Raul");
//    person2.getCity().setNameCity("Chile");
//
//    System.out.println(person);
//    System.out.println(person2);


    }
}
