package principal;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.Security;
import java.security.spec.KeySpec;

public class Brasil {

    private static final String ALGORITHM = "AES";
    private static final String SECRET_KEY = "2F787E7D4B7D3E34505A6471385E404A2C453D7B7660753A4D332D5256456B6E5F3864287778544E763A5B422567757E5D7B5F5079796B214A734047282D7948747573782D512A5C3425365C3F75582C7A57232C58595A4D7D254E3D477874784C7223257A60434A4127377070327D29445724797546236A425B33626E3D3476";


    public static String decrypt(String encryptedData) {
        try {
            Security.addProvider(new BouncyCastleProvider());

            byte[] keyBytes = SECRET_KEY.getBytes();
            SecretKey secretKey = new SecretKeySpec(keyBytes, 0, 32, ALGORITHM); // Utilizamos los primeros 32 bytes de la clave

            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            byte[] decryptedData = cipher.doFinal(hexStringToByteArray(encryptedData));

            return new String(decryptedData);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static void main(String[] args) {

        String nombre = "16 B\n" +
                "00000000  4C C8 F0 7F EC 5C 9D 4A 58 0C 36 83 07 A3 D2 A9    LÈð.ì\\.JX.6..£Ò©\n";

        String response = decrypt(nombre);

        System.out.println("Response = " + response);

    }
}
