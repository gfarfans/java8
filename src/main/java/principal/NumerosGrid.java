package principal;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.util.Random;

public class NumerosGrid extends JFrame {

    public NumerosGrid() {
        setTitle("Números en Grid");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel numerosPanel = new JPanel(new GridLayout(8, 6, 10, 30)); // Panel para los botones de números

        // Agregar los números como botones circulares al panel de números
        for (int i = 1; i <= 48; i++) {
            CircularButton button = new CircularButton(String.valueOf(i));
            button.setFont(new Font("Arial", Font.PLAIN, 20)); // Ajustar el tamaño de la fuente
            button.setFocusPainted(false); // Eliminar el resaltado al obtener el foco

            // Agregar el evento para cambiar el color cuando se seleccione
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (button.isSelected()) {
                        button.setBackground(new Color(220, 220, 220));
                        button.setSelected(false);
                    } else {
                        button.setBackground(Color.RED);
                        button.setSelected(true);
                    }
                }
            });

            numerosPanel.add(button);
        }

        JPanel textPanel = new JPanel(new GridLayout(3, 1)); // Panel para el área de texto
        JTextArea textArea1 = new JTextArea();
        JTextArea textArea2 = new JTextArea();
        JTextArea textArea3 = new JTextArea();
        textPanel.add(textArea1);
        textPanel.add(textArea2);
        textPanel.add(textArea3);

        // Contenedor principal con BorderLayout
        JPanel containerPanel = new JPanel(new BorderLayout());
        containerPanel.add(numerosPanel, BorderLayout.CENTER);

        // Contenedor para el panel de números y el panel de texto
        JPanel contentPanel = new JPanel(new BorderLayout());
        contentPanel.add(containerPanel, BorderLayout.CENTER);
        contentPanel.add(textPanel, BorderLayout.LINE_END);

        getContentPane().add(contentPanel); // Agregar el contenedor al JFrame

        setSize(800, 550); // Ajustar el tamaño de la ventana
        setLocationRelativeTo(null); // Centrar la ventana en la pantalla
    }

    // Clase para botones circulares
    class CircularButton extends JButton {

        public CircularButton(String text) {
            super(text);
            setContentAreaFilled(false); // No llenar el área interior
            setBorderPainted(false); // No pintar el borde
            setOpaque(false); // No es opaco
            setPreferredSize(new Dimension(80, 80)); // Establecer el tamaño preferido para proporcionar espacio adicional
        }

        @Override
        protected void paintComponent(Graphics g) {
            if (getModel().isArmed()) {
                g.setColor(Color.RED); // Color cuando se presiona el botón
            } else {
                g.setColor(getBackground());
            }
            g.fillOval(0, 0, getSize().width - 1, getSize().height - 1); // Dibujar un círculo
            super.paintComponent(g);
        }

        @Override
        protected void paintBorder(Graphics g) {
            // No pintar el borde
        }

        Shape shape; // Forma del botón

        @Override
        public boolean contains(int x, int y) {
            if (shape == null || !shape.getBounds().equals(getBounds())) {
                shape = new Ellipse2D.Float(0, 0, getWidth(), getHeight());
            }
            return shape.contains(x, y);
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            NumerosGrid numerosGrid = new NumerosGrid();
            numerosGrid.setVisible(true);
        });
    }
}
