package principal;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VentanaSplit {

    public static List<String> getFileContent(String path, String fileName) {

        String pathFileName = path + fileName;

        System.out.println("Obteniendo contenido de archivo = " + pathFileName);

        List<String> stringList = new ArrayList<>();

        try {

            BufferedReader b = new BufferedReader(new InputStreamReader(new FileInputStream(pathFileName),
                    "UTF-8"));

            stringList = b.lines().filter(x -> !x.trim().equals("")).collect(Collectors.toList());

            b.close();
        } catch (Exception e) {
            System.out.println("No se pudo leer el contenido del archivo " + pathFileName);
            System.exit(1);
        }

        return stringList;
    }

    public static String getNum02Digits(int numero) {
        return String.format("%02d", numero);
    }

    public static void main(String[] args) {
        // Crear el marco principal
        JFrame frame = new JFrame("Ventana Dividida");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 300);

        // Crear la lista de cadenas con los elementos "1 3 4 5 7 10" y "2 5 6 8 1 20"
        List<String> datos = getFileContent("D:/data.csv", "");
//        datos.add("1 3 4 5 7 10");
//        datos.add("2 5 6 8 1 20");


        // Crear el panel superior y agregar los 48 botones
        JPanel panelSuperior = new JPanel(new GridLayout(8, 6, 5, 5)); // GridLayout de 8 filas y 6 columnas con espaciado de 5 píxeles
        List<JButton> buttons = new ArrayList<>();
        for (int i = 1; i <= 48; i++) {
            JButton button = new JButton(getNum02Digits(i));
            buttons.add(button); // Agregar botón a la lista
            panelSuperior.add(button); // Agregar botón al panel
        }

        // Crear el panel inferior y agregar el JList con los datos
        JList<String> jList = new JList<>(datos.toArray(new String[0]));
        JPanel panelInferior = new JPanel(new BorderLayout());
        panelInferior.add(new JScrollPane(jList), BorderLayout.CENTER); // Agregar un JScrollPane para permitir el desplazamiento

        // Agregar un ListSelectionListener al JList para manejar la selección
        jList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) { // Solo actuamos cuando la selección está establecida
                    int selectedIndex = jList.getSelectedIndex();
                    if (selectedIndex != -1) { // Seleccionado un elemento
                        String selectedValue = datos.get(selectedIndex); // Obtener el valor seleccionado del JList
                        String[] numbers = selectedValue.split(" "); // Dividir el valor en números individuales
                        for (JButton button : buttons) {
                            if (Arrays.asList(numbers).contains(button.getText())) { // Si el número del botón está en la lista seleccionada
                                button.setBackground(Color.RED);// Cambiar el color del botón
                                button.setForeground(Color.WHITE); // Cambiar el color del texto del botón a blanco
                            } else {
                                button.setBackground(null); // Restaurar el color original del botón
                                button.setForeground(Color.BLACK); // Restaurar el color de texto original del botón

                            }
                        }
                    }
                }
            }
        });

        // Crear el divisor (split pane) y agregar los paneles superior e inferior
        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, panelSuperior, panelInferior);
        splitPane.setDividerLocation(150); // Posición del divisor

        // Agregar el divisor al marco principal
        frame.add(splitPane);

        // Mostrar la ventana
        frame.setVisible(true);
    }
}

