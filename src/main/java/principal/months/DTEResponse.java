package principal.months;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {})
@XmlRootElement(name = "DTE_RESPONSE", namespace = "http://www.amdocs.com/amdd/stp/GBF")
public class DTEResponse {

    @XmlValue
    protected String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static void main(String[] args) throws Exception {
        JAXBContext context = JAXBContext.newInstance(DTEResponse.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
        DTEResponse dteResponse = new DTEResponse();
        dteResponse.setValue("<TED version=\"1.0\"><DD><RE>76124890-1</RE><TD>33</TD><F>68669286</F><FE>2023-04-01</FE><RR>99500140-3</RR><RSR>EKA CHILE SA</RSR><MNT>2210939</MNT><IT1>Cargo Fijo</IT1><CAF version=\"1.0\"><DA><RE>76124890-1</RE><RS>TELEFONICA MOVILES CHILE S.A.</RS><TD>33</TD><RNG><D>68518851</D><H>68818850</H></RNG><FA>2023-01-25</FA><RSAPK><M>4WY330Oel6l5emyEjJQL52H/bm2KsEcZEW00iz/0nMrRUZwV+kNJ9Itqn2p55xsS/aPTr/0nEl4EopTjjFDZ+w==</M><E>Aw==</E></RSAPK><IDK>300</IDK></DA><FRMA algoritmo=\"SHA1withRSA\">WaU+NTlTmAxj9dGh9XA8SHhlvJ8iMFOyUcje9v09Fion6EREi4uxFEAoLCo/lC1NdkaoXxUqwMmWC6KBhs45uA==</FRMA></CAF><TSTED>2023-04-03T01:57:18</TSTED></DD><FRMT algoritmo=\"SHA1withRSA\">L4EGmtD9wWcC0FYm90NEUvUt3aoUwazzONfU87QzSyMa1N8yagYGg5x1GchBYT29V20du1i3+j60 4YxPaJ2esA==</FRMT></TED>");
        marshaller.marshal(dteResponse, System.out);
    }
}

