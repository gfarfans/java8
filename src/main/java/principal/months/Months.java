package principal.months;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.IntStream;

public class Months {

  private BiFunction<DateTime, DateTime, Integer> calculate() {
    return (open, now) -> {
      int cant = ((12*(now.getYear() - open.getYear())) + now.getMonthOfYear() - open.getMonthOfYear());
      return cant >= 24 ? 24 : cant;
    };
  }



  public static void main(String[] ars) {

    Months app = new Months();

    DateTime now = new DateTime();
    System.out.println("now :" + now);

    String date = "2018-3-25";
    String pattern = "yyyy-MM-dd";

    DateTime open = DateTime.parse(date, DateTimeFormat.forPattern(pattern));

    List<String> list = new ArrayList<>();

    Integer months = app.calculate().apply(open, now);

    IntStream it = IntStream.range(1, months+1);
    it.forEach(x -> {
      DateTime r = now.plusMonths(-x);
      list.add(r.getMonthOfYear() + "-" + r.getYear());
    });

    System.out.println(list.toString());
    System.out.println(list.size());
  }
}
